<?php

namespace Dnhb\Jwt\Claim;

use Dnhb\Jwt\Claim;

class Audience extends Claim
{
	protected string $type = 'aud';
    protected string $name = 'audience';
}

<?php

namespace Dnhb\Jwt\Claim;

class NotBefore extends \Dnhb\Jwt\Claim
{
	protected string $type = 'nbf';
	protected string $name = 'notBefore';

	public function validate($value): bool
    {
		return is_numeric($value);
	}
}

<?php

namespace Dnhb\Jwt\Claim;

use Dnhb\Jwt\Claim;

class Issuer extends Claim
{
	protected string $type = 'iss';
    protected string $name = 'issuer';
}

<?php

namespace Dnhb\Jwt\Claim;

use Dnhb\Jwt\Claim;

class Subject extends Claim
{
	protected string $type = 'sub';
    protected string $name = 'subject';
}

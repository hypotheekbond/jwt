<?php

namespace Dnhb\Jwt\Claim;

use Dnhb\Jwt\Claim;

class Custom extends Claim
{
	protected string $type;
    protected string $name = 'custom';
}

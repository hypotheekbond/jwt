<?php

namespace Dnhb\Jwt\Claim;

class JwtId extends \Dnhb\Jwt\Claim
{
	protected string $type = 'jti';
    protected string $name = 'jwtId';
}

<?php

namespace Dnhb\Jwt\Claim;

use Dnhb\Jwt\Claim;

class IssuedAt extends Claim
{
	protected string $type = 'iat';
	protected string $name = 'issuedAt';

	public function validate($value): bool
	{
		return is_numeric($value);
	}
}

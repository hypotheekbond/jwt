<?php

namespace Dnhb\Jwt\Claim;

use Dnhb\Jwt\Claim;

class Type extends Claim
{
	protected string $type = 'typ';
    protected string $name = 'type';
}

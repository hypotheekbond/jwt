<?php

namespace Dnhb\Jwt\Claim;

class ExpireTime extends \Dnhb\Jwt\Claim
{
	protected string $type = 'exp';
	protected string $name = 'expireTime';

	public function validate($value): bool
	{
		return is_numeric($value);
	}
}

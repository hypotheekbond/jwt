<?php

namespace Dnhb\Jwt;

use Countable;
use Iterator;

class ClaimsCollection implements Countable, Iterator
{
    /**
     * Set of Claims
     * @var Claim[]
     */
    private array $claims = [];

    public function add(Claim $claim): static
    {
        $this->claims[] = $claim;
        return $this;
    }

    public function toArray(): array
    {
        $data = array();
        foreach ($this->claims as $claim) {
            $data[$claim->getType()] = $claim->getValue();
        }
        return $data;
    }

    private int $position = 0;

    public function count(): int
    {
        return count($this->claims);
    }

    public function current(): mixed
    {
        return $this->claims[$this->position];
    }

    public function key(): mixed
    {
        return $this->position;
    }

    public function next(): void
    {
        ++$this->position;
    }

    public function rewind(): void
    {
        $this->position = 0;
    }

    public function valid(): bool
    {
        return isset($this->claims[$this->position]);
    }
}

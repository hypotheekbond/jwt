<?php

namespace Dnhb\Jwt;

abstract class HashMethod
{
    abstract public function getAlgorithm(): int|string;

    public function isValidKey($key): bool
    {
        if (is_resource($key) === false) {
            $key = openssl_pkey_get_public($key) ?: openssl_pkey_get_private($key);
            if (!$key) {
               return false;
            }
        }
        $details = openssl_pkey_get_details($key);

        return isset($details['key']) && $this->getKeyType() === $details['type'];
    }

    public function getKeyType(): int|string|null
    {
        return $this->keyType;
    }
}

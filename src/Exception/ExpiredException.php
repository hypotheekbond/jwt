<?php

namespace Dnhb\Jwt\Exception;

class ExpiredException extends DecodeException {}

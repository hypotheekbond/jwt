<?php

namespace Dnhb\Jwt\Exception;

class BadSignatureException extends DecodeException {}

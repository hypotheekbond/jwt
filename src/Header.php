<?php

namespace Dnhb\Jwt;

use JsonException;

class Header
{
    private string $type = 'JWT';
    private string $algorithm = 'HS256';
    private string $key;

    private array $hashTypes = array(
        'HS256' => 'SHA256',
        'HS384' => 'SHA384',
        'HS512' => 'SHA512',
    );

    public function __construct(string $key, string $algorithm = 'HS256', string $type = 'JWT')
    {
        $this->setType($type);
        $this->setAlgorithm($algorithm);
        $this->setKey($key);

        // If it's a certificate, we're a JWS instead
        if (is_resource($key)) {
            $this->setType('JWS');
        }
    }

    public function setType($type): static
    {
        $this->type = $type;
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setAlgorithm(string $algorithm): static
    {
        $this->algorithm = $algorithm;
        return $this;
    }

    public function getAlgorithm($resolve = false): mixed
    {
        $algorithm = $this->algorithm;
        if ($resolve === true) {
            foreach ($this->hashTypes as $key => $algo) {
                if ($key === $algorithm) {
                    return $this->hashTypes[$key];
                }
            }
        }

        return $algorithm;
    }

    public function setKey($key): static
    {
        $this->key = $key;
        return $this;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    /** @throws JsonException */
    public function __toString(): string
    {
        $data = $this->toArray();

        return json_encode($data, JSON_THROW_ON_ERROR);
    }

    public function toArray(): array
    {
        return [
            'typ' => $this->getType(),
            'alg' => $this->getAlgorithm()
        ];
    }
}

<?php

namespace Dnhb\Jwt;

use Dnhb\Jwt\Claim\Custom;
use InvalidArgumentException;
use JsonException;
use RuntimeException;
use stdClass;

use function function_exists;
use function hash_equals;
use function strlen;

/**
 * Class Jwt
 *
 * @method Jwt issuer (string $url) Sets the issuer claim
 * @method Jwt issuedAt (int $time) Sets the issuedAt claim
 * @method Jwt notBefore (int $time) Sets the notBefore claim
 * @method Jwt expireTime (int $time) Sets the expireTime claim
 */
class Jwt
{
    private ClaimsCollection $claims;
    private Header $header;

    public function __construct(Header $header, ClaimsCollection $collection = null)
    {
        $this->setHeader($header);
        $this->setClaims($collection ?? new ClaimsCollection());
    }

    public function setHeader(Header $header): static
    {
        $this->header = $header;

        return $this;
    }

    public function getHeader(): Header
    {
        return $this->header;
    }

    public function addClaim(Claim $claim): static
    {
        $this->claims->add($claim);

        return $this;
    }

    public function getClaims(): ClaimsCollection
    {
        return $this->claims;
    }

    public function setClaims(ClaimsCollection $collection): static
    {
        $this->claims = $collection;

        return $this;
    }

    /** @throws JsonException */
    public function encode(string $claims = null, bool $addIssued = false): string
    {
        $header = $this->getHeader();
        $claimData = $this->getClaims()->toArray();

        // If we don't have an "issued at" make one
        if (!isset($claimData['iat']) && $addIssued === true) {
            $claimData['iat'] = time();
        }
        ksort($claimData);

        $claims = $claims ?? $this->base64Encode(
            json_encode($claimData, JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES)
        );

        $headerData = $header->toArray();
        ksort($headerData);

        $sections = [
            $this->base64Encode(json_encode($headerData, JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES)),
            $claims,
        ];

        $signWith = implode('.', $sections);
        $signature = $this->sign($signWith, $header->getKey());

        $sections[] = $this->base64Encode($signature);

        return implode('.', $sections);
    }

    /** @throws Exception\BadSignatureException|Exception\DecodeException|JsonException */
    public function decode(string $data, bool $verify = true): stdClass
    {
        $sections = explode('.', $data);
        if (count($sections) < 3) {
            throw new Exception\DecodeException('Invalid number of sections (<3)');
        }

        [$header, $claims, $signature] = $sections;
        $header = json_decode($this->base64Decode($header), false, 512, JSON_THROW_ON_ERROR);
        $claims = json_decode($this->base64Decode($claims), false, 512, JSON_THROW_ON_ERROR);
        $signature = $this->base64Decode($signature);
        $key = $this->getHeader()->getKey();

        return ($verify === true) && $this->verify($key, $header, $claims, $signature, $sections[0] . '.' . $sections[1]) === false
            ? throw new Exception\BadSignatureException('Signature did not verify')
            : $claims;
    }

    /** @throws RuntimeException|JsonException */
    public function encrypt(string $algorithm, string $iv, string $key): string
    {
        if (!function_exists('openssl_encrypt')) {
            throw new RuntimeException('Cannot encrypt data, OpenSSL not enabled');
        }

        $data = json_encode($this->getClaims()->toArray(), JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES);
        $claims = $this->base64Encode(
            openssl_encrypt(
                $data,
                $algorithm,
                $key,
                false,
                $iv
            )
        );

        return $this->encode($claims);
    }

    /** @throws Exception\DecodeException|RuntimeException|JsonException */
    public function decrypt(string $data, string $algorithm, string $iv, string $key): stdClass
    {
        if (!function_exists('openssl_encrypt')) {
            throw new RuntimeException('Cannot encrypt data, OpenSSL not enabled');
        }

        $sections = explode('.', $data);
        if (count($sections) < 3) {
            throw new Exception\DecodeException('Invalid number of sections (<3)');
        }

        $claims = openssl_decrypt(
            $this->base64Decode($sections[1]),
            $algorithm,
            $key,
            false,
            $iv
        );
        $decodedClaims = json_decode($claims, false, 512, JSON_THROW_ON_ERROR);

        $header = json_decode($this->base64Decode($sections[0]), false, 512, JSON_THROW_ON_ERROR);

        if ($this->verify(
                $this->header->getKey(),
                $header,
                $decodedClaims,
                $this->base64Decode($sections[2]),
                $sections[0] . '.' . $sections[1]
            ) === false
        ) {
            throw new Exception\BadSignatureException('Signature did not verify');
        };

        return $decodedClaims;
    }

    /** @throws Exception\DecodeException|Exception\ExpiredException|Exception\DecodeException|Exception\DecodeException */
    public function verify($key, $header, $claims, $signature, $signWith): bool
    {
        if (empty($header->alg)) {
            throw new Exception\DecodeException('Invalid header: no algorithm specified');
        }

        if (isset($claims->aud) && empty($claims->aud)) {
            throw new Exception\DecodeException('Audience cannot be empty [aud]');
        }

        // If "expires at" defined, check against time
        if (isset($claims->exp) && $claims->exp <= time()) {
            throw new Exception\ExpiredException('Message has expired');
        }

        // If a "not before" is provided, validate the time
        if (isset($claims->nbf) && $claims->nbf > time()) {
            throw new Exception\DecodeException(
                'Cannot process prior to ' . date('m.d.Y H:i:s', $claims->nbf) . ' [nbf]'
            );
        }

        return $this->hash_equals($this->sign($signWith, $key), $signature);
    }

    public function base64Encode(string $data): string
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    public function base64Decode(string $data): string
    {
        $decoded = str_pad(
            $data,
            4 - (strlen($data) % 4),
            '='
        );

        return base64_decode(strtr($decoded, '-_', '+/'));
    }

    public function sign(string $signWith, string $key): string
    {
        $hashType = $this->getHeader()->getAlgorithm();

        $hashClass = '\\Dnhb\\Jwt\\HashMethod\\' . $hashType;

        if (class_exists($hashClass) === false) {
            throw new InvalidArgumentException('Invalid hash type: ' . $hashType);
        }

        $hash = new $hashClass();
        if ($hash->getKeyType() === 'HMAC') {
            $signature = hash_hmac($hash->getAlgorithm(), $signWith, $key, true);
        } else {
            if ($hash->isValidKey($key) === false) {
                throw new \Dnhb\Jwt\Exception\InvalidKeyException('Invalid key provided');
            }

            openssl_sign($signWith, $signature, $key, $hash->getAlgorithm());
        }

        return $signature;
    }

    /** @throws InvalidArgumentException */
    public function __call(string $name, array $args): static
    {
        $className = "\\Dnhb\\Jwt\\Claim\\" . ucwords($name);

        if (class_exists($className)) {
            $claim = new $className($args[0]);
            $this->addClaim($claim);

            return $this;
        }

        throw new InvalidArgumentException('Invalid claim type "' . $name . '"');
    }

    public function __get(string $name): mixed
    {
        foreach ($this->getClaims() as $claim) {
            if ($claim->getName() === $name) {
                return $claim->getValue();
            }
        }

        return null;
    }

    public function custom(array|string $value, string $name = null): static
    {
        $value = !is_array($value) ? [$name => $value] : $value;

        foreach ($value as $type => $val) {
            $claim = new Custom($val, $type);
            $this->addClaim($claim);
        }

        return $this;
    }

    public function hash_equals($a, $b): bool
    {
        return hash_equals($a, $b);
    }
}

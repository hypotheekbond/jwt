<?php

namespace Dnhb\Jwt;

use DomainException;

abstract class Claim
{
    protected string $type;

    protected string $value;

    protected string $name;

    public function __construct(string $value, string $type = null)
    {
        $this->setValue($value);

        if ($type) {
            $this->setType($type);
        }
    }

    /** @throws DomainException */
    public function setValue(string $value): static
    {
        if (method_exists($this, 'validate') && !$this->validate($value)) {
            throw new DomainException(
                'Invalid data provided for claim "' . $this->getType() . '": ' . $value
            );
        }

        $this->value = $value;

        return $this;
    }

    public function setType($type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function toArray(): array
    {
        return [
            'value' => $this->getValue(),
            'type' => $this->getType(),
        ];
    }

    public function getName(): string
    {
        return $this->name;
    }
}

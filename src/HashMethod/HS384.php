<?php

namespace Dnhb\Jwt\HashMethod;

use Dnhb\Jwt\HashMethod;

class HS384 extends HashMethod
{
    protected string $keyType = 'HMAC';

    public function getAlgorithm(): int|string
    {
        return 'SHA384';
    }
}

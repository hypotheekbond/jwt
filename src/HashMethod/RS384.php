<?php

namespace Dnhb\Jwt\HashMethod;

use Dnhb\Jwt\HashMethod;

class RS384 extends HashMethod
{
    protected int $keyType = OPENSSL_KEYTYPE_RSA;

    public function getAlgorithm(): int|string
    {
        return extension_loaded('openssl')
            ? OPENSSL_ALGO_SHA384
            : throw new \InvalidArgumentException('You cannot use this hashing method without OpenSSL support');
    }
}

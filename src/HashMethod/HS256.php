<?php

namespace Dnhb\Jwt\HashMethod;

class HS256 extends \Dnhb\Jwt\HashMethod
{
	protected string $keyType = 'HMAC';

	public function getAlgorithm(): int|string
    {
		return 'SHA256';
	}
}

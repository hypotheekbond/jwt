<?php

namespace Dnhb\Jwt\HashMethod;

use Dnhb\Jwt\HashMethod;

class RS512 extends HashMethod
{
    protected int $keyType = OPENSSL_KEYTYPE_RSA;

    public function getAlgorithm(): int|string
    {
        return extension_loaded('openssl')
            ? OPENSSL_ALGO_SHA512
            : throw new \InvalidArgumentException('You cannot use this hashing method without OpenSSL support');
    }
}

<?php

namespace Dnhb\Jwt\HashMethod;

use Dnhb\Jwt\HashMethod;

class HS512 extends HashMethod
{
	protected string $keyType = 'HMAC';

	public function getAlgorithm(): int|string
    {
		return 'SHA512';
	}
}
